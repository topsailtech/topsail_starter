$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "topsail_starter/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "topsail_starter"
  s.version     = TopsailStarter::VERSION
  s.authors     = ["Jan Schroeder"]
  s.email       = ["info@topsailtech.com"]
  s.homepage    = "http://www.topsailtech.com"
  s.summary     = "Stuff we need for our upcoming Rails4 apps."
  s.description = "Stuff we need for our upcoming Rails4 apps."

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "> 4.0.0"
  s.add_dependency "will_paginate"
  s.add_dependency 'devise'
  s.add_dependency 'topsail_will_filter'
  s.add_dependency 'topsail_audit', ">= 1.4"
  s.add_dependency "rails-assets-select2"

  #s.add_development_dependency "sqlite3"
end
