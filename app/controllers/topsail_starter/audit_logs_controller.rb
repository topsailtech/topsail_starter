module TopsailStarter
  class AuditLogsController < ::ApplicationController

    include ActsResourcy
    include SecuredResource

    alias_method :index_original, :index
    def index

      index_original
      @records = @records.includes(:created_by)

    end

    def show
      @audit_log = AuditLog.find(params[:id])
      authorize @audit_log
      @prior_audit_logs = AuditLog.where(['audited_id = ? AND audited_type = ? AND id < ?', @audit_log.audited_id, @audit_log.audited_type, @audit_log.id])
      @later_audit_logs = AuditLog.where(['audited_id = ? AND audited_type = ? AND id > ?', @audit_log.audited_id, @audit_log.audited_type, @audit_log.id])
      unless @audit_log.is_a?(AssociationAuditLog)
        @audited_attributes_after_action = JSON.parse(@audit_log.attributes_json || '{}')
        @changes = @audit_log.record_changes || {}
      end
      @audited_current = @audit_log.audited
    end

    def show_history
      @audited_record = params[:audited_type].constantize.find(params[:audited_id])
      authorize @audited_record

      @audit_logs = AuditLog.where(audited_type: @audited_record.class.name, audited_id: @audited_record ).
                             order('created_at desc')

      render layout: false

    end

  end
end
