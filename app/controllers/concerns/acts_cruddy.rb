module ActsCruddy

  extend ActiveSupport::Concern

  included do

    helper_method :can?
    helper_method :accessible_records

    def new
      set_record_variables
      authorize! @record
    end

    def show
      set_record_variables
      authorize! @record
    end

    def edit
      set_record_variables
      authorize! @record
    end

    def index
      @records_unpaginated ||= record_class.filter_by(params[:filter]).
                                            sorted_by(params[:sort])

      authorize! @records_unpaginated.model
      @records_unpaginated = accessible_records(@records_unpaginated)

      paginate_settings = {:page=>1}.merge(params[:paginate]&.to_unsafe_h || {}).symbolize_keys
      if jump_to = params[:will_sort] && params[:will_sort].delete(:jump_to)
        paginate_settings[:page] = @records_unpaginated.rank_for_value(jump_to) / @records_unpaginated.per_page + 1
      end

      response.headers['Vary'] = 'Accept' # to deal with https://code.google.com/p/chromium/issues/detail?id=94369 (back button retrieves latest XHR instead of page)

      @records = @records_unpaginated.paginate( paginate_settings )
    end

    def create
      set_record_variables
      authorize! @record

      if @record.save
        flash[:notice] = response.headers["Content-Description"] = hierarchical_translate('record_created', :name => record_name_value(@record))
        response.headers["Location"] = url_for(:action => :show, :id => @record)
        # should return status :created, but I don't want to force a #render call here
      else
        render 'create_failure', :status => :unprocessable_entity
      end

    end

    def update
      set_record_variables
      authorize! @record

      if @record.update_attributes(permitted_params)
        flash[:notice] = response.headers["Content-Description"] = hierarchical_translate('record_updated', :name => record_name_value(@record))
      else
        render 'update_failure', :status => :unprocessable_entity
      end

    end

    def destroy
      set_record_variables
      authorize! @record

      @record.destroy

      if @record.destroyed?
        flash[:notice] = response.headers["Content-Description"] = hierarchical_translate('record_destroyed', :name => record_name_value(@record))
        # should usually return status :accepted (no response content), but I don't want to force a #render call here
      else
        render 'destroy_failure', :status => :unprocessable_entity
      end

    end


    protected

    #
    #  Any of the methods below could be overwritten in subclasses, but
    #     #permitted_params MUST be overwritten
    #

    def record_name_value(record)
      "#{ record.class.model_name.human } #{ record.respond_to?(:name) ? "\"#{ record.send(:name) }\"" : '' }"
    end

    def set_record_variables
      @record = params[:id].present? ? record_class.find(params[:id]) : record_class.new(permitted_params)
    end

    #
    # overwrite the following methods to implement your security!
    #
    def permitted_params
      raise "your controller must implement #permitted_params"
    end

    def authorize!(resource)
      raise "your controller must implement #authorize! (e.g. by including Pundit)" unless respond_to?( :authorize!, true)
      super
    end

    def accessible_records(scope)
      raise "your controller must implement #accessible_records, e.g. via Pundit by saying policy_scope(scope)"
    end

    def can?(action, resource)
      raise "your controller must implement #can?, e.g. via Pundit by saying policy(resource).send(action + '?')" unless respond_to?(:can?, true)
      super
    end

    public # just in case there will be more later, we don't want to shadow this


  end

  module ClassMethods
  end

  private

  # The scope of a translation by default is based on the location of the
  # template file.  This provides a version of translate that will look
  # for a value based on the application_controller subclass name, then fall back to the
  # application_controller scope, regardless of where the template file is.
  def hierarchical_translate(key, options={})

    options = {
      :scope => controller_path.gsub('/', '.') + '.' + action_name,
      :default => t("application.#{action_name}.#{key}",
                      { :human_name => record_class.model_name.human,
                        :plural_human_name => record_class.model_name.human(count: 2)
                      }.merge(options))
    }.merge(options)

    t(key, options)

  end

  def record_class
    @record_class ||= controller_name.classify.constantize
  end


end