module ActsResourcy

  extend ActiveSupport::Concern

  included do

    include ActsCruddy

    layout :find_layout

    private
    def find_layout
      request.xhr? ? false : nil
    end

  end

  module ClassMethods
    # to support view inheritance from modules

    if Rails.version.to_f >= 4.2

      def local_prefixes # :nodoc:
        answer = [controller_path]
        # find which superclass is the first one to include the ActsResourcy module and insert those views into the view search path right before there
        answer << 'topsail_starter/resources' unless self.superclass.included_modules.include?(ActsResourcy)
        answer
      end

    else

      def parent_prefixes
        unless @parent_prefixes
          # find which superclass is the first one to include the ActsResourcy module and insert those views into the view search path right before there
          s_class, path_idx = self, 0
          path_idx += 1 while (s_class = s_class.superclass) && s_class.included_modules.include?(ActsResourcy)
          @parent_prefixes = super.insert(path_idx,'topsail_starter/resources')
        end
        @parent_prefixes
      end

    end

  end


end
