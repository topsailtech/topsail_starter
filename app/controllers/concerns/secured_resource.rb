#
# This Concern implements the API that connects acts_cruddy with the underlaying Authorization framework.
#
# Include this concern in your contraller that uses acts_cruddy!
#
#  acts_cruddy requires the following methods to be available in the controller:
#       - authorize!
#       - accessible_records(scope)
#       - permitted_params
#       - can?(action, resource)
#
module SecuredResource

  extend ActiveSupport::Concern

  included do

    # in case some controller uses SecuredResource w/o ActsCruddy
    helper_method :can?
    helper_method :accessible_records

    ####
    ####     Pundit
    ####
    if Gem::Specification.find_by_name('pundit')

      def accessible_records(scope)
        policy_scope(scope)
      end

      def permitted_params
        record_name = ActiveModel::Naming.param_key(record_class).to_sym
        params[record_name] ? policy(@record || record_class).permitted_params(params.require(record_name)) : {}
      end

      def can?(action, resource)
        policy(resource).send("#{action}?")
      end

      def authorize!(resource)
        authorize resource
      end

    end

  end

  module ClassMethods
  end

end