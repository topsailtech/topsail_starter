class DataTable < ViewComponent

  #
  # Accessors
  #
  attr_accessor :records # not needed for #tr action

  #
  # public actions
  #
  def tr(record)
    render partial: 'tr', locals:{record: record}
  end

  #
  # Stuff to overwrite in subclasses
  #
  def column_definitions
    raise "Subclass Responsibility"
  end

  def router
    @calling_context # could also be e.g. @calling_context.topsail_starter_router
  end

  def totals
    nil
  end

  def show
    render 'table'
  end

  #
  # helpers
  #
  protected
  helper_method :router, :records, :column_keys, :cell_procs, :th, :num_columns,
                :will_sort_jump_to, :totals


  def column_keys
    column_definitions.keys
  end

  def cell_procs
    column_definitions.values
  end


  def num_columns
    column_definitions.size
  end


  def th(attribute)
    link_inner = records.nil? || attribute.blank? ? attribute : records.model.human_attribute_name(attribute)

    content_tag :th do
      if records.sortable_by?(attribute)
        root_context.ts_loadable_sorter(records, attribute, title: link_inner)
      else
        link_inner
      end
    end
  end

  def will_sort_jump_to(records)
    if records.sorted_by_hash.any? && records.total_pages > 2 && [:string, :numeric].include?(records.primary_sort_type)
      content_tag :span, style: 'float: left; margin-left:3em', class: 'jump_to' do
        content_tag :label do
          safe_join([
            "Jump To #{ records.model.human_attribute_name(records.sorted_by_hash.keys.first) } ",
            link_to('jump_to_trigger', router.url_for(params.to_unsafe_h.merge({will_sort_placeholder: 'placeholder'})), style:'display:none'),
            tag(:input, name:"will_sort[jump_to]", size:2,
                onchange:raw("$s=$(this).prev(); $s.attr('href', $s.attr('href').replace('will_sort_placeholder=placeholder','will_sort[jump_to]='+ this.value)); $s[0].click()"))
          ])
        end
      end
    else
      "".html_safe
    end
  end


end
