# TODO:  figure out how to include all the app's helpers, not just the one ApplicationHelper
class ViewComponent < ActionController::Base

    include ActionView::Context # to allow content_tag etc. access to the output_buffer and somehow the pagination limit doesn't work without this
    include ActionView::Helpers::OutputSafetyHelper # for raw (and html_safe??? or h()? At least in Rails 4.2 h() is not there anymore), and safe_join
    include ActionView::Helpers::UrlHelper # for link_to() and somehow the pagination limit doesn't work without this

    # AbstractController::Helpers would give us access to the application's helpers. Use root_context.gaga instead, or include this Helper in the
    #    component's subclass when needed

    # forward key methods to the calling controller
    extend Forwardable
    def_delegator :@calling_context, :request
    def_delegator :@calling_context, :url_for

    # some topsail_starter (SecuredResource) helpers
    def_delegator :@calling_context, :accessible_records
    def_delegator :@calling_context, :can?
    helper_method :accessible_records, :can?

    self.view_paths = ["app/view_components", File.expand_path(File.dirname(__FILE__)) ]

    def initialize(calling_context, locals={})

      super()

      @locals = locals

      # help forwardable, and make instance variables available
      @calling_context = calling_context
      @__root_context = calling_context.instance_variable_get(:@__root_context) || calling_context # the original active controller/view

    end


    #
    #  actions
    #      by default, to_s will render the "show" template so that you don't actually have to call any action
    #
    def to_s
      show
    end

    # default implementation; Roll your own if you want to!
    def show
      render 'show'
    end

    # I would like to hide this variable, and just have :render and :local_render available, but I can't get this going....
    def root_context
      @__root_context
    end
    helper_method :root_context

    protected

    def render(*opts)
      render_to_string(*opts).html_safe
    end
    def content_type # for Rails 4.2
      :html
    end

end
