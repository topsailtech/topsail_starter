class DataTableAjaxy < DataTable

  # if xhr, we need the 'records' local
  def show
    render partial: (request.xhr? ? 'table' : 'initial_placeholder')
  end

  #
  # helpers/getters
  #
  helper_method :url, :fetch_on_page_load, :filter_form_id

  def url
    @locals[:url] || request.params.merge({filter: {}, format: :html})
  end

  def filter_form_id
    @locals[:filter_form_id] || 'new_filter'
  end

  # should the datatable immediately load? Overwrite in subclasses if undesired
  def fetch_on_page_load
    true
  end

end
