module TopsailStarter
  module ResourcesTableHelper

    def resource_index_table_component(resource_class)

      answer = nil
      r_class = resource_class
      while !answer && r_class != ActiveRecord::Base do
        answer = begin; "#{r_class.name}IndexTable".constantize.new(self); rescue; end
        r_class = r_class.superclass
      end

      unless answer # fallback if we don't have a specific IndexTable component
        column_defs_ary = resource_class.attribute_names.map{ |name| [name, ->(r){ r.attributes[name] }] }
        answer = GenericDataTable.new(self).tap{ |c| c.column_definitions = Hash[column_defs_ary] }
      end

      answer

    end

    def ts_loadable_sorter(records, attribute, title: records.model.human_attribute_name(attribute))
      current_multi_sort_index = records.sort_index(attribute) || -1

      link_inner = current_multi_sort_index > 0 ?
                        safe_join([title, content_tag(:span, current_multi_sort_index + 1, class: 'sort_position')]) :
                        title

      atts = {name: attribute}
      case records.sort_dir(attribute)
        when :asc
          atts['sort-pos'] = records.sort_index(attribute) + 1
        when :desc
          atts['sort-pos'] = records.sort_index(attribute) + 1
          atts[:desc] = true
      end
      content_tag 'ts-loadable-sorter', link_inner, atts

    end

  end
end