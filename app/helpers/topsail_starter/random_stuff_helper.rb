module TopsailStarter
  module RandomStuffHelper

    # based on http://laktek.com/2012/10/04/extract-values-from-a-string/
    # example:
    # extract_values_as_hash("/2012/08/12/test.html", "/{year}/{month}/{day}/{title}.html")
    #      {"year"=>"2012", "month"=>"08", "day"=>"12", "title"=>"test"}
    def extract_values_as_hash(str, pattern)
      delimiters = ["{", "}"]
      special_chars_regex = /[\\\^\$\*\+\.\?\(\)]/

      token_regex = Regexp.new( delimiters[0] + "([^" + delimiters.join("") + "\t\r\n]+)" + delimiters[1])
      tokens = pattern.gsub(token_regex).to_a   # => ["{year}", "{month}", "{day}", "{title}"]
      pattern_regex = Regexp.new((pattern.gsub(special_chars_regex){|m| "\\" + m}).gsub(token_regex, "(\.+)")) # RegExp /\/(.+)\/(.+)\/(.+)\/(.+)\.html/
      matches = str.scan(pattern_regex).first

      return nil if matches.blank?

      output = {};
      key_regex = Regexp.new(delimiters[0] + "|" + delimiters[1])
      tokens.each_with_index{ | token, i |
        output[token.gsub( key_regex, "")] = matches[i];
      }
      return output;

    end

    # color format as described in function #rgb
    # formula from http://en.wikipedia.org/wiki/Relative_luminance
    # 0xFFFFFF => luminance 1;   0x000000 => luminance 0
    def perceptive_luminance(color)
      rgb = rgb(color)
      ( 0.2126 * rgb[0] + 0.7152 * rgb[1] + 0.0722 * rgb[2] ) / 255
    end

    # For lighten, amount should be a decimal between 0 and 1. Higher means lighter.
    # For darken, amount should be a decimal between 0 and -1. Lower means darker.
    #  Inspired by http://www.redguava.com.au/2011/10/lighten-or-darken-a-hexadecimal-color-in-ruby-on-rails/
    def lighten_color(color, amount=0.3)
      rgb = rgb(color)
      if amount >= 0
        rgb[0] = [(rgb[0].to_i + 255 * amount).round, 255].min
        rgb[1] = [(rgb[1].to_i + 255 * amount).round, 255].min
        rgb[2] = [(rgb[2].to_i + 255 * amount).round, 255].min
      else
        amount = 1 + amount
        rgb[0] = (rgb[0].to_i * amount).round
        rgb[1] = (rgb[1].to_i * amount).round
        rgb[2] = (rgb[2].to_i * amount).round
      end
      "#%02x%02x%02x" % rgb
    end

    private
    # num_color_or_hex_color can be hex_color string ("#12ab00") or number (0xf199a0)
    def rgb(num_color_or_hex_color)
      if num_color_or_hex_color.is_a?(String)
        num_color_or_hex_color.gsub('#','').scan(/../).map {|color| color.hex}
      else
        rgb(num_color_or_hex_color.to_s(16))
      end
    end

  end

end