/* Disable standard select2 until version 4.0.2 comes out */
//*= require select2.js


document.registerElement('ts-select2', {
      extends: 'select',
      prototype: Object.create(HTMLSelectElement.prototype, {
        attachedCallback: {

          value: function() {

            var sel2_opts = {},
                first_option = this.options[0];

            // Set allow-clear and placeholder
            if (!this.hasAttribute("data-allow-clear") && first_option && first_option.value ==''){
              this.setAttribute("data-allow-clear", true);
            }

            if (!this.hasAttribute("data-placeholder") && first_option && first_option.value ==''){
              this.setAttribute("data-placeholder", first_option.innerHTML);
            }

            //
            // Set Ajax default options
            //
            if (this.hasAttribute("data-ajax--url")){

              sel2_opts['ajax'] = {
                data: this._data_fn.bind(this),
                processResults: (
                                  WC.function_named(this.getAttribute("data-ajax--processResults")) ||
                                  this._processResults_fn
                                ).bind(this)
              };
              sel2_opts['escapeMarkup']      = function (markup) { return markup; }; // let our custom formatter work
              sel2_opts['templateSelection'] = this._ajax_templateSelection_fn;
              sel2_opts['templateResult']    = this._ajax_templateResult_fn;

              if (!this.hasAttribute("data-ajax--delay")) this.setAttribute("data-ajax--delay", 250);
              if (!this.hasAttribute("data-minimum-input-length")) this.setAttribute("data-minimum-input-length", 2);

              this.per_page = this.getAttribute("data-per-page") || 20;
              this.search_key = this.getAttribute("data-search-key") || 'q';
            }

            //
            //  Expose a few select2 options that are not (yet?) fully exposed via data attributes
            //
            if (this.hasAttribute("data-dropdown-parent-selector")){
              sel2_opts['dropdownParent'] = $(this.getAttribute("data-dropdown-parent-selector"));
            }
            // HACK for <dialog> element, since Top Layer makes the dropdown inaccessible
            if (!sel2_opts['dropdownParent']){
              var ancestor_dialog = this.closest("dialog");
              if (ancestor_dialog){ sel2_opts['dropdownParent'] = $(ancestor_dialog) }
            }


            //
            //  Translate JQuery's custom "change" event to a regular CustomEvent (otherwise we can not attach a
            //    plain event listener). Be aware that the onchange attribute now will be invoked for the light-weight
            //    JQuery change event AND the actual chnage CustomEvent!
            //
            $(this).on("change", function(e){
              if (e.isTrigger){
                WC.fireEvent(this, "change");
                return false;
              }
            });


            //
            //  Initialize the JQuery Select2 control
            //
            if (document.readyState == 'loading'){ // we don't have enough information about the select's style yet => wait
              $(document).ready(function(){
                $(this).select2(sel2_opts);
                this._init_drag_and_drop();
              }.bind(this));
            } else {
              $(this).select2(sel2_opts);
              this._init_drag_and_drop();
            };


            //
            //  Support for data-count-start and data-count-label
            //
            if (this.hasAttribute("data-count-start")){
              $(this).on("change", function(e){
                var $selection_rendered = $(this.nextElementSibling.querySelector('ul.select2-selection__rendered')),
                    cnt = $(this).find("option:selected").length;
                if (cnt >= this.getAttribute("data-count-start")){
                  $selection_rendered.find("li.select2-selection__choice").remove();
                  $selection_rendered.prepend("<li class='select2-selection__choice select2__count'>(" + cnt +
                     " " + (this.getAttribute("data-count-label") || "items") + ")</li>");
                }
              })
            };
          }
        },




        _data_fn: {
          value: function(params){ // page is the one-based page number tracked by Select2
            var req_params = {filter: {},  paginate: {page: params.page, per_page: this.per_page}};
            req_params['filter'][this.search_key] = params.term;
            return req_params;
          }
        },

        _processResults_fn: {
          value: function(data, params){
            params.page = params.page || 1;
            return { results: data.records, pagination: {more: (params.page * this.per_page) < data.total}  };
          }
        },

        // what to show once a row in the drop down was selected (record is what came back from Ajax)
        // templateSelection must work for AJAX results AND for possible initial selected OPTION
        _ajax_templateSelection_fn: {
          value: function(record){
            return record.short_html || record.html || record.text;
          }
        },

        // rows in the select2 dropdown from ajax result records
        _ajax_templateResult_fn: {
          value: function(record){
            return record.long_html || record.html || record.text;
          }
        },


        //
        //
        //     DND
        //
        //
        _init_drag_and_drop: {
          value: function(){
            if (this.hasAttribute("data-orderable")){

              this.sortable_list = this.nextElementSibling.querySelector('ul.select2-selection__rendered');

              // make the current list itmes draggable
              // we have to make sure this gets re-done each time the list is re-generated
              $(this).on("select2:open change.select2", function(e){
                [].forEach.call(this.sortable_list.children, function(li){ li.draggable = true });
              }.bind(this));

              // preserve order of insertion
              $(this).on('select2:select', function(e){
                  $(this).append($(e.params.data.element)).trigger('change.select2');
              });

              // to allow DROP event to fire in Chrome
              this.sortable_list.addEventListener("dragover", function(e){ e.preventDefault() });

              // Remembering an element that will be move
              this.sortable_list.addEventListener("dragstart", function(e){
                this.dragged_element = e.target; // e.dataTransfer only remember strings, but we want to know the DOM element
              }.bind(this), false);

              this.sortable_list.addEventListener("drop", function(e){
                var target = e.target;
                if( target && target !== this.dragged_element && target.nodeName == 'LI' ){
                  var dropped_on_opt = $(target).data("data").element,
                      dragged_opt = $(this.dragged_element).data("data").element;
                  $(dropped_on_opt).after(dragged_opt);
                  $(this).trigger('change.select2');
                }
              }.bind(this));

            }
          }
        }

      })
    });

/*
    TopsailStarter.Select2 = {
      colorFormatter: function(selection){
        return "<div style='background-color:" + selection.text + "'>" + selection.text + "</div>"
      }
    };
*/