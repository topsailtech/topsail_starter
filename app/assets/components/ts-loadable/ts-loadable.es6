/*
  This uses JQuery!

  attributes:   href
                insert (optional) - "top" or "bottom", inserts resulting content rather than
                                    replace the entire element content
                extra-params (optional) - JSON of initial extra_params
                lazyload (optional)     - don't immediately load the content
                extra-params-form-selector (optional) - a selector string identifying a form element, that will be used as
                                                        property 'extra_params_form' (see below)

  properties:  extra_params - a plain object passed in to subsequent AJAX load requests;
                              when setting, you can provide a plain object or a "railsy" request string (the setter converts the string to an object)
               extra_params_form - a form dom element; on submit of the form, the extra_params are being replaced with the form's
                                   current input values, and the ts-loadable will be reloaded

  methods: load() - forces the element to fetch new content via AJAX, using it's @href and #extra_params
           update_extra_params(param_name, value) - sets the value of existing extra_params'
                                                    key identified by param_name (key
                                                    can be complex like "filter[id][]")

  events: ts-loaded - fired once the content is updated after a load()

  interactivity cues: ts-loadable.loading - class appended during load(); cleared when content is updated

*/


class TsLoadable extends HTMLElement {

  connectedCallback(){
    // set initial extra_params
    this.extra_params = this.hasAttribute("extra-params") ?
                            JSON.parse(this.getAttribute("extra-params")) :
                            {};
    if (this.hasAttribute("extra-params-form-selector")){
      var f = document.querySelector(this.getAttribute("extra-params-form-selector").replace(/\//g,"\\/"));
      if (f){
        this.extra_params_form = f;
      }
    }

    // load on startup unless told otherwise
    if (!this.hasAttribute("lazyload")){ this.load(); }
  }

  get extra_params(){
    return this._extra_params;
  }

  set extra_params(string_or_object){ // url_request params or hash/object
    this._extra_params = typeof string_or_object == "string" ?
                                    WC.serialize_as_object(string_or_object) :
                                    string_or_object;
  }

  update_extra_params(param_name, value){
    if (!(typeof this._extra_params == "object")) {
      this._extra_params = {};
    }
    WC.railsy_update_property_hash(this._extra_params, param_name, value);
  }

  get extra_params_form(){
    return this._extra_params_form;
  }

  set extra_params_form(form_el){
    this._extra_params_form = form_el;
    this.extra_params = WC.serialize_as_string(form_el); // in case the form has some pre-sets
    form_el.addEventListener("submit",function(){
        this.extra_params = WC.serialize_as_string(form_el);
        this.load();
    }.bind(this));
  }

  load(force){
    var el = this;

    // allow listeners to prevent variant change
    if ( !force && !WC.fireEvent(this, "ts-load") ){ return false; }

    el.classList.add("loading");
    el._current_jqxhr && el._current_jqxhr.abort(); // kill possible pending request

    this._current_jqxhr = WC.$get(this.getAttribute('href'), this.extra_params, function(responseText, textStatus, jqXHR){

      el._current_jqxhr = null;

      if (el.getAttribute("insert") == "top") {
        $(el).prepend(responseText);
      } else if (el.getAttribute("insert") == "bottom") {
        $(el).append(responseText);
      } else {
        $(el).html(responseText); // use jquery instead of innerHTML so that JS gets executed
      }

      // After updating the element's content, store some additional data
      var finder = /^X-TS-(.*?):[ \t]*([^\r\n]*)\r?$/mgi, // IE leaves an \r character at EOL
          ts_response_headers = {},
          match;
      while ( (match = finder.exec( jqXHR.getAllResponseHeaders() )) ) {
        ts_response_headers[ match[1].toLowerCase() ] = match[ 2 ];
      }
      el.ts_response_headers = ts_response_headers;

      WC.fireEvent(el, "ts-loaded");
      el.classList.remove("loading");
    });
  }
};

customElements.define('ts-loadable', TsLoadable);