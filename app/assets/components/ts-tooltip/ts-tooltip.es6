class TsTooltip extends HTMLElement {

  connectedCallback(){
    WC.setup_dom_template(this, "#ts-tooltip");

    var tip = this;
    this.hoverable = this.previousElementSibling;

    this.hoverable.addEventListener("mouseover", tip._show_deferred.bind(tip));
    this.hoverable.addEventListener("mouseout",  tip.hide.bind(tip));

    this.addEventListener("mouseover", tip.show.bind(tip));
    this.addEventListener("mouseout",  tip.hide.bind(tip));

    // clicks inside the tooltip shouldn't bleed out
    this.addEventListener("click", function(e){ e.stopPropagation();} );
  }

  hide(){
    this.classList.remove('hovering');
    this.show_delay && clearTimeout(this.show_delay); // in case there is a showing pending
  }

  show(){

    // allow listeners to prevent variant change
    if (!this.hasAttribute("href") || !this.loaded) {
      if ( !WC.fireEvent(this, "ts-before-content-change") ){
        return false;
      }
    };

    if (this.hasAttribute("href") && !this.loaded){

      WC.$get(this.getAttribute('href'), this.extra_params, function(responseText, textStatus, jqXHR){
        this.querySelector(".tip-msg").innerHTML = responseText;
        this.loaded = true;

        if (this.hoverable.matches(":hover")){ // only if we should still show it (loading took time!)
          this.show(this);
        }
      }.bind(this));
      return; // the Ajax reply will trigger the actual showing of the element
    };

    // show
    this.classList.add('hovering');
    if (this.hasAttribute("always-reload")){
      this.loaded = false;
    }

    var hoverable_abs_offset = WC.absolute_offset(this.hoverable),
        hoverable_absolute_scroll = {x:0, y:0};// WC.absolute_scroll(this.hoverable);
    // Set vertical position
    var tip_height = this.offsetHeight,
        top_delta = hoverable_abs_offset.top - hoverable_absolute_scroll.y < tip_height ?
                             this.hoverable.offsetHeight :
                             0 - tip_height;
    this.style.top = hoverable_abs_offset.top - hoverable_absolute_scroll.y + top_delta + "px";
    this.classList.toggle('bottom', top_delta > 0);

    // Set horizontal position
    var tip_width = this.offsetWidth,
        left_delta = 0;
    // if tip sticks out to the right of the parent rectangle, try to right-align it with hoverable
    if (hoverable_abs_offset.left - hoverable_absolute_scroll.x + tip_width > this.offsetParent.offsetWidth){
      left_delta = 0 - tip_width + this.hoverable.offsetWidth;
    }
    // if it now sticks out to the left of the parent rectangle, see if it fits on the screen to the right
    if (hoverable_abs_offset.left - hoverable_absolute_scroll.x + left_delta < 0 &&
        this.hoverable.getBoundingClientRect().left + tip_width < document.body.getBoundingClientRect().right){
      left_delta = 0;
    }
    this.style.left = hoverable_abs_offset.left - hoverable_absolute_scroll.x + left_delta + "px";
    this.classList.toggle('left', left_delta < 0);

  }

  _show_deferred(){
    this.show_delay = setTimeout(this.show.bind(this), this.getAttribute("delay") || 0)
  }


};

customElements.define('ts-tooltip', TsTooltip);
