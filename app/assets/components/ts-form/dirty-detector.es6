/*
    <ts-form-dirty-detector></ts-form-dirty-detector>

    Just add

          <ts-form-dirty-detector></ts-form-dirty-detector>

   anywhere inside a form.

    attributes:  disabled (optional) -  if present, don't do dirty detection. (Use JS to enable dirty detection if needed)

    properties: disabled - boolean; set/get disabled attribute
                dirty    - getter if the form is currently dirty

    TODO: remove dirty detection of the form/dirty-detector is being remioved form the DOM.

*/
class TsFormDirtyDetector extends HTMLElement {

  connectedCallback(){
    this.form = WC.closest(this, "form");
    this.form.dirty_detector = this; // so that we can find a form's dirty-detector quickly

    this._memorize();

    // seems like that the _window_global_unload_handler will only get registered once (good!)
    window.addEventListener('beforeunload', this._window_global_unload_handler)
  }

  get disabled(){
    return this.hasAttribute("disabled");
  }

  set disabled(_val){
    if (this.disabled == _val) return;
    if (_val) {
      this.setAttribute("disabled","")
    } else {
      this.removeAttribute("disabled")
    }
  }

  reset(){
    this._memorize()
  }

  get dirty(){
    var answer = false,
        current_nodelist = this._currentNodeList;
    if (current_nodelist.length != this.memorizedNodeList.length){
      answer = true;
    } else {
      for(var j = current_nodelist.length; j > 0; j--){
        var memorized_item = this.memorizedNodeList[j - 1],
            current_node = current_nodelist[j - 1];
        if(memorized_item.name != current_node.name || memorized_item.value != current_node.value){
          answer = true;
          break;
        }
      }
    }
    return answer;
  }

  //
  //    Private
  //

  // a live nodelist
  get _currentNodeList(){
    return this.form.querySelectorAll("input, select, textarea")
  }

  _memorize(){
    this.memorizedNodeList = [].map.call(this._currentNodeList, function(i){ return {name: i.name, value: i.value} })
  }

  _window_global_unload_handler(e){
    var msg = '';
    msg = [].reduce.call(document.querySelectorAll("ts-form-dirty-detector"), function(msg, detector){
      return msg || (!detector.disabled && detector.dirty)
    }, msg);

    if (msg){
      e.returnValue = msg;
    }
    return msg;
  }

};

customElements.define('ts-form-dirty-detector', TsFormDirtyDetector);