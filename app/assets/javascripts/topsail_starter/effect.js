var TopsailStarter = TopsailStarter || {};

TopsailStarter.Effect = {

  highlight: function(el){

    var $el = $(el);
    $el.before("<div/>");

    $el.prev()
        .width($el.width())
        .height($el.height())
        .css({
            "position": "absolute",
            "background-color": "#ffff99",
            "opacity": ".9"
        })
       //.fadeOut(500, $.fn.remove);
       .fadeOut(500, function(){
            this.remove();
       });
  }

}