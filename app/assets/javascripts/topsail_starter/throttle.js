var TopsailStarter = TopsailStarter || {};

// from Spectrum (https://github.com/bgrins/spectrum); License: MIT
// If the 'debounce' flag is set the function only fires after inactivity.
// Example: $("#f").on("keyup", TopsailStarter.throttle(function(e){ console.log(e) }))
TopsailStarter.throttle = function(func, wait, debounce) {
    var timeout;
    return function () {
        var context = this,
            args = arguments;
        var throttler = function () {
            timeout = null;
            func.apply(context, args);
        };
        if (debounce) clearTimeout(timeout);
        if (debounce || !timeout) timeout = setTimeout(throttler, wait);
    };
};