module TopsailStarter
  module AttributeSanitizer
    module ActiveRecordExtension
      extend ActiveSupport::Concern

      included do
        METHOD_HOLDER = TopsailStarter::AttributeSanitizer
        def sanitized_value(sanitizers, value)
          sanitizers.inject(value){ |run, sanitizer_sym|
            if run.nil?
              nil
            elsif METHOD_HOLDER.respond_to?("should_#{sanitizer_sym}?")
              METHOD_HOLDER.send("should_#{sanitizer_sym}?", run) ? METHOD_HOLDER.send(sanitizer_sym, run) : run
            else
              METHOD_HOLDER.send sanitizer_sym, run
            end
          }
        end
      end

      module ClassMethods
        def sanitize_attribute(attributes, *sanitizers)

          Array(attributes).each do |attribute|
            before_validation do |record|

              change = record.changes[attribute]

              if(change)
                was, is = change[0], change[1]
                if was.present? && is.present?
                  sanitized_was = self.sanitized_value(sanitizers, was)
                  sanitized_is = self.sanitized_value(sanitizers, is)
                  record.send("#{attribute}=", sanitized_was != sanitized_is ? sanitized_is : was)
                elsif !is.nil?
                  sanitized_is = self.sanitized_value(sanitizers, is)
                  record.send("#{attribute}=", sanitized_is)
                end
              end

            end
          end

        end
      end
    end

    def self.should_titleize_if_monocase?(value)
      value == value.upcase || value == value.downcase
    end

    def self.titleize_if_monocase(value)
      value.titleize
    end

    def self.titleize(value)
      value.titleize
    end

    def self.upcase(value)
      value.upcase
    end

    def self.downcase(value)
      value.downcase
    end

    def self.strip(value)
      value.strip
    end

    def self.blank_to_nil(value)
      value.blank? ? nil : value
    end

    def self.nbsp_character_to_space(value)
      # https://weblogs.java.net/blog/2008/01/11/%C3%A3%E2%80%9A-and-nbsp-mystery-explained
      # replace ascii 160 (A0 hex) with 32 (20 hex)
      value.gsub(/\xC2\xA0/, ' ')
    end

    def self.html_nbsp_to_space(value)
      value.gsub(/\&nbsp\;/, ' ')
    end

  end

end

ActiveRecord::Base.send :include, TopsailStarter::AttributeSanitizer::ActiveRecordExtension