module TopsailStarter
  module MigrationExtensions
    module TableDefinitions

      def audit_cols

        timestamps

        # add columns
        #    note: the :foreign_key option might not do anything in your DB (unless you are using Oracle Enhanced Adapter or Rails >= 4.2.1)

        # add constraints
        if (4.2...5).include? Rails.version.to_f
          references :created_by, :null => false, foreign_key: {table_name: 'user_profile'}
          references :updated_by, :null => false, foreign_key: {table_name: 'user_profile'}
        else
          references :created_by, :class_name => 'UserProfile', :null => false
          references :updated_by, :class_name => 'UserProfile', :null => false
          foreign_key :user_profile, column: 'created_by_id', name: "#{name}_cr_by_fk"
          foreign_key :user_profile, column: 'updated_by_id', name: "#{name}_ub_by_fk"
        end
      end

    end
  end
end


if defined?(ActiveRecord::ConnectionAdapters::TableDefinition)
   ActiveRecord::ConnectionAdapters::TableDefinition.send :include, TopsailStarter::MigrationExtensions::TableDefinitions
end