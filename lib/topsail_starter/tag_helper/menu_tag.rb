#
# Rules for menu_structure:
#     - must be Array
#     - if Array element is nil, it will be ignored
#     - if Array element is a Class, it will generate a menu entry to the :index action of the class, named according i18n
#     - if Array element is a Hash, where first value is not an Array, use
#              a) first key as menu entry name
#              b) first value as menu link url
#              c) append all remaining key/value pairs to the menu link, except for key :li_atts
#              d) appends all entries of the (optional) :li_atts value hash to the menu link's parent LI element
#     - if Array element is a Hash and first value is an Array, use key as menu link name, and create a submenu out of
#              the first value array. Use all remaining key/value pairs for the li tag
#     - if Array element is an html safe string, just copy it over into the menu unchanged


module TopsailStarter
  module TagHelper
    module MenuTag

      def menu_tag(menu_structure)

        menu_entries = []

        menu_structure.each{ |item|
          if item.nil? || item == false
            # skip false and nil
          elsif item.is_a? Class
            menu_entries << content_tag(:li, link_to(item.model_name.human(count:2), item))
          elsif item.is_a?(Hash) && item.any?
            k1, v1 = item.keys.first, item.values.first
            if v1.is_a?(Array) # submenu
              menu_entries << content_tag(:li, item.slice!(k1)) do
                content_tag(:h2, k1) + menu_tag(v1)
              end
              #TODO - check this out. Mae: add a 3rd option to ck if submenu is a hash here?
            else  # named menu entry
              menu_entries << content_tag(:li, link_to(k1, v1, item.slice!(k1, :li_atts)), item.slice(:li_atts).values.first)
            end
          elsif item.is_a?(String) && item.html_safe?
            menu_entries << item
          end
        }


        menu_entries.empty? ? nil : content_tag(:ul, safe_join(menu_entries))

      end

    end
  end
end

ActionView::Base.send :include, TopsailStarter::TagHelper::MenuTag