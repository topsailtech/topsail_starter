module TopsailStarter
  module FormBuilder
    class ReadOnlyBuilder < ActionView::Helpers::FormBuilder

      #
      #
      #   TopsailStarter extensions
      #
      #
      def autocomplete(method, *other_opts)
        show_text(method, __method__)
      end

      def select2(method, choices, options = {}, html_options = {})
        selected_choice = choices.detect{|c| (c.respond_to?(:last) ? c.last : c).to_s  == @object.send(method).to_s}
        show_text(selected_choice ? (selected_choice.respond_to?(:first) ? selected_choice.first : selected_choice) : '', __method__)
      end

      def tag_select(method, choices, options = {}, html_options = {})
        show_text(method, __method__)
      end

      def link_to_add_fields(*opts)
      end

      def link_to_remove_fields(*opts)
      end

      #
      #
      #   FormOptionsHelper
      #
      #
      def select(method, choices, options = {}, html_options = {})
        selected_choice = choices.detect{|c| c.last.to_s == @object.send(method).to_s}
        show_text(selected_choice ? selected_choice.first : '', __method__)
      end

      def collection_select(method, collection, value_method, text_method, options = {}, html_options = {})
      end

      def grouped_collection_select(method, collection, group_method, group_label_method, option_key_method, option_value_method, options = {}, html_options = {})
      end

      def time_zone_select(method, priority_zones = nil, options = {}, html_options = {})
        show_text(method, __method__)
      end

      def collection_check_boxes(method, collection, value_method, text_method, options = {}, html_options = {}, &block)
      end

      def collection_radio_buttons(method, collection, value_method, text_method, options = {}, html_options = {}, &block)
      end

      #
      #   Standard FormBuilder stuff
      #
      #
      def text_field(method, *other_opts)#, options = {})
        show_text(possible_value(other_opts.first) || method.to_sym, __method__)
      end

      def text_area(method, *other_opts)#, options = {})
        method_or_value = possible_value(other_opts.first) || method.to_sym
        display_val = method_or_value.is_a?(Symbol) ? @object.send(method_or_value) : method_or_value
        # must h() the string BEFORE passing to simple_format as it apparently eats un-escaped characters
        show_text(@template.simple_format(ERB::Util.h(display_val)), __method__)
      end

      def hidden_field(method, *other_opts)
      end

      def  check_box(method, options = {}, checked_value = "1", unchecked_value = "0")
        options[:disabled] = true
        super
      end

      def password_field(method, *other_opts)
      end

      def file_field(method, *other_opts)
      end

      def radio_button(method, tag_value, options={})
        options[:disabled] = true
        super
      end

      def color_field(method, *other_opts)
      end

      def search_field(method, *other_opts)
      end

      def telephone_field(method, *other_opts)
        show_text(method.to_sym, __method__)
      end

      def phone_field(method, *other_opts)
        show_text(method.to_sym, __method__)
      end

      def date_field(method, *other_opts)
        show_text(method.to_sym, __method__)
      end

      def time_field(method, *other_opts)
        show_text(method.to_sym, __method__)
      end

      def datetime_field(method, *other_opts)
        show_text(method.to_sym, __method__)
      end

      def datetime_local_field(method, *other_opts)
        show_text(method.to_sym, __method__)
      end

      def month_field(method, *other_opts)
        show_text(method.to_sym, __method__)
      end

      def week_field(method, *other_opts)
        show_text(method.to_sym, __method__)
      end

      def url_field(method, *other_opts)
        show_text(method.to_sym, __method__)
      end

      def email_field(method, *other_opts)
        show_text(method.to_sym, __method__)
      end

      def number_field(method, *other_opts)
        show_text(method.to_sym, __method__)
      end

      def range_field(method, *other_opts)
        show_text(method.to_sym, __method__)
      end

      def submit(value, *other_opts)
      end

      def button(value, *other_opts)
      end

      #
      #
      private
      # if method_or_value is a symbol, assume this to be a method, otherwise it is a value
      def show_text(method_or_value, css_class)
        display = method_or_value.is_a?(Symbol) ? @object.send(method_or_value) : method_or_value
        # h() the return string to ensure embedded sequences are escaped
        @template.content_tag(:span, ERB::Util.h(display), class: css_class)
      end

      # return the value of key :value. Only return nil if the key doesn't exist
      def possible_value(opt_hash)
        if opt_hash && opt_hash.include?(:value)
          v = opt_hash[:value]
          v || ''
        else
          nil
        end
      end
    end
  end
end