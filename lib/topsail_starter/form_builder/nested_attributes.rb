module TopsailStarter
  module FormBuilder
    module NestedAttributes

      include ActionView::Helpers::JavaScriptHelper

      # parameters:
      #              - name :  just like first attribute for "regular" link_to function)
      #              - association : for which attribute fields to created the nested fields
      #              - options :  - :partial : the partial to use for rendering the nested fields form. Defaults to #{association.to_s.singularize}_fields
      #                           - :form_builder_name : the variable name of the form builder in the partial. Defaults to :f
      #                           - :nested_object : the empty object used for building the form; defaults to object.send(association).build (this
      #                                              is useful if the nested form is not for a real AR assiciation, or if you wnat to set certain defaults)
      #                           - :locals : an (optional) hash whose elements get passed through as locals to the :partial
      #                           - container_jq_selector : the JQuery selector for the container where the new form should be appended. Defaults to '$(this).prev()'
      #                           - fields_added : callback function after fields are added. The only argument for this fn is the array of root nodes of the added DOM fragment;
      #                                              e.g.  :fields_added => 'function(row){console.log(row)}'
      #                           - action_view_template : defaults to @template
      #                           - all other options are being passed through to link_to as html_options
      def link_to_add_fields(name, association, options={})
        partial = options.delete(:partial) || (association.to_s.singularize + "_fields")
        form_builder_name = options.delete(:form_builder_name) || :f
        container_jq_selector = options.delete(:container_jq_selector) || '$(this).prev()'
        fields_added_js = options.delete(:fields_added)
        partial_locals = options.delete(:locals) || {}
        nested_object = options.key?(:nested_object) ? options.delete(:nested_object) : object.send(association).build
        action_view_template = options.delete(:action_view_template) || @template

        form_builder_placeholder = "new_#{association}_PLACEHOLDER"
        fields = fields_for(association, nested_object, :child_index => form_builder_placeholder) do |builder|
          partial_locals[form_builder_name] = builder
          action_view_template.render(partial, partial_locals)
        end

        js = "var t=$(\"#{escape_javascript(fields)}\".replace(new RegExp('#{form_builder_placeholder}', 'g'), new Date().getTime())); #{container_jq_selector}.append(t);"
        js += "(#{fields_added_js})(t)" unless fields_added_js.blank?
        js += ";return false"
        options[:onclick] = js

        action_view_template.link_to(name, '#', options)
      end

      # parameters:
      #              - name :  just like first attribute for "regular" link_to function)
      #              - options :  - container_jq_selector : the JQuery selector for the container that should be hidden when clicking. Defaults to '$(this).parent()'
      #                           - before_fields_remove : callback function before removing field; must return true in order to proceed with field removal; no args, but 'this' will point to the link element
      #                           - fields_removed : callback function after fields are removed; no arguments for this fn; the 'this' variable will point to the link element
      #                           - all other options are being passed through to link_to as html_options
      def link_to_remove_fields(name, options={})
        container_jq_selector = options.delete(:container_jq_selector) || '$(this).parent()'
        fields_removed_js = options.delete(:fields_removed)
        before_fields_remove_js = options.delete(:before_fields_remove)

        js = ""
        js += "if ( !( #{before_fields_remove_js} ).apply(this) ) return false;" unless before_fields_remove_js.blank?
        js += "$(this).prev('input[type=hidden]')[0].value=1; #{container_jq_selector}.hide();"
        js += "(#{fields_removed_js}).apply(this)" unless fields_removed_js.blank?
        js += ";return false"
        options[:onclick] = js

        hidden_field(:_destroy) + @template.link_to(name, '#', options)
      end

    end
  end
end

ActionView::Helpers::FormBuilder.send :include, TopsailStarter::FormBuilder::NestedAttributes
