class TsResourceGenerator < Rails::Generators::NamedBase

  class_option :parent_class,      type: :string, default: "ActiveRecord::Base",  desc: "The parent class name for the resource"
  class_option :parent_controller, type: :string, default: "ResourcesController", desc: "The parent class name for the controller"
  class_option :parent_policy,     type: :string, default: "ApplicationPolicy",   description: "The parent class name for the policy"

  source_root File.expand_path('../templates', __FILE__)

  def create_ts_resource_files
    template 'model.rb', File.join('app/models', class_path, "#{file_name}.rb")
    template 'controller.rb', File.join('app/controllers', class_path, "#{file_name.pluralize}_controller.rb")
    template 'policy.rb', File.join('app/policies', class_path, "#{file_name}_policy.rb")
    template 'view_component.rb', File.join('app/view_components', class_path, "#{file_name}_index_table.rb")
    copy_file '_index_filter.html.erb', File.join('app/views', class_path, "#{file_name.pluralize}/_index_filter.html.erb")
    copy_file '_form.html.erb', File.join('app/views', class_path, "#{file_name.pluralize}/_form.html.erb")

    route "resources :#{file_name.pluralize} # adjust the module if this resource is in a module!"
  end

  protected
    def parent_class_name
      options[:parent_class]
    end

    def parent_controller_name
      options[:parent_controller]
    end

    def parent_policy_name
      options[:parent_policy]
    end
end
