<% module_namespacing do -%>
class <%= class_name.pluralize %>Controller < <%= parent_controller_name.classify %>
end
<% end -%>