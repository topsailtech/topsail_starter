<% module_namespacing do -%>
class <%= class_name %>Policy < <%= parent_policy_name.classify %>

  # def create?
  #   false
  # end

  # def permitted_params(resource_params)
  #   resource_params.permit(:name)
  # end

  class Scope < Scope
    def resolve
      super
    end
  end

end
<% end -%>