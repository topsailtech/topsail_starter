<% module_namespacing do -%>
class <%= class_name %> < <%= parent_class_name.classify %>

  will_sort default: 'id'

  will_filter id: ->(v){ where id: v }

end
<% end -%>
