How To add vendor libs
======================

1) Copy the entire folder into this directory here
2) make sure the images are in some library specific subdirectory, so that their path is namespaced
3) rename the lib's CSS file to .scss
4) edit the lib's SCSS and replace all url() references with something like image-url('myLib/notice.png')
5) Add an initilaizer to this project with line
    Rails.application.config.assets.precompile += %w( myLib/*.png )
  that way we will get properly fingerprinted image files in production!