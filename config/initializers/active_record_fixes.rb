if (4.2...5).include? Rails.version.to_f
  #
  # Rails 4.2.1 removed the t.foreign_key thingy and allowed adding a foreign_key: true option to the column t.references options.
  #    Unfortunately this only seems to work with pluralized table names, where the column name and the table name match.
  #    Patching this so that we can pass in a foreign_key: {table_name: referenced_table_name} option
  #
  module ActiveRecord
    module ConnectionAdapters
      class TableDefinition
        def references(*args)
          options = args.extract_options!
          polymorphic = options.delete(:polymorphic)
          index_options = options.delete(:index)
          foreign_key_options = options.delete(:foreign_key)
          type = options.delete(:type) || :integer

          if polymorphic && foreign_key_options
            raise ArgumentError, "Cannot add a foreign key on a polymorphic relation"
          end

          args.each do |col|
            column("#{col}_id", type, options)
            column("#{col}_type", :string, polymorphic.is_a?(Hash) ? polymorphic : options) if polymorphic
            index(polymorphic ? %w(type id).map { |t| "#{col}_#{t}" } : "#{col}_id", index_options.is_a?(Hash) ? index_options : {}) if index_options

            # START Topsail Patch
            if foreign_key_options && foreign_key_options.is_a?(Hash) && foreign_key_options[:table_name]
              foreign_key(foreign_key_options[:table_name], foreign_key_options.merge(column: "#{col}_id"))
            elsif foreign_key_options
              to_table = Base.pluralize_table_names ? col.to_s.pluralize : col.to_s
              foreign_key(to_table, foreign_key_options.is_a?(Hash) ? foreign_key_options : {})
            end
            # END Topsail Patch
          end
        end
      end

      module SchemaStatements
        def add_reference(table_name, ref_name, options = {})
          polymorphic = options.delete(:polymorphic)
          index_options = options.delete(:index)
          type = options.delete(:type) || :integer
          foreign_key_options = options.delete(:foreign_key)

          if polymorphic && foreign_key_options
            raise ArgumentError, "Cannot add a foreign key to a polymorphic relation"
          end

          add_column(table_name, "#{ref_name}_id", type, options)
          add_column(table_name, "#{ref_name}_type", :string, polymorphic.is_a?(Hash) ? polymorphic : options) if polymorphic
          add_index(table_name, polymorphic ? %w[type id].map{ |t| "#{ref_name}_#{t}" } : "#{ref_name}_id", index_options.is_a?(Hash) ? index_options : {}) if index_options
          if foreign_key_options && foreign_key_options.is_a?(Hash) && foreign_key_options[:table_name]
            add_foreign_key(table_name, foreign_key_options[:table_name], foreign_key_options.merge(column: "#{ref_name}_id"))
          elsif foreign_key_options
            to_table = Base.pluralize_table_names ? ref_name.to_s.pluralize : ref_name
            add_foreign_key(table_name, to_table, foreign_key_options.is_a?(Hash) ? foreign_key_options : {})
          end
        end
      end

    end

  end
end