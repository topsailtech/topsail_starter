if defined?(AuditLog) && AuditLog.table_exists?

  class AuditLog < ActiveRecord::Base

    scopes_for_date(:created_at)

    will_filter created_at_on_or_after:          ->(t){ created_at_on_or_after(t) },
                created_at_on_or_before:         ->(t){ created_at_on_or_before(t) }

    will_sort default:        'created_at DESC',
              created_at:     'audit_log.created_at'

    def audited_inspect
      self.audited&.inspect || "#{self.audited_type} ##{self.audited_id} (deleted)"
    end

  end

end